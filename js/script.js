//Menu:
//On menu hover in.
function hoverIn(){
	this.style.backgroundColor = "#7c97b3";
	this.style.borderRadius = "5px";
}

//On menu hover out.
function hoverOut(){
	this.style.backgroundColor = "#333333";
}



//mobHeader toggle button
function toggle(){
     var content = document.getElementById('menuContent');
	 if(content.style.display=="none"){
			content.style.display="block";
		}
		else{
			content.style.display="none";
		}
}

function formSuccess(){
	var email = document.getElementById('email').value;
	var name = document.getElementById('name').value;
	var ta =document.getElementById('ta').value;
	
	if(email && name && ta != "")
	{
		alert("Message was sent successfully!");
		email == "";
		name =="";
		ta == "";
	}
}



//onLoad
window.onload = function(){
	var toggleButt = document.getElementById('toggleButt');
	toggleButt.onclick = toggle;
	
	
	var listItem = document.getElementsByClassName('listItem');
	for(var i=0; i<listItem.length; i++){
		listItem[i].onmouseover = hoverIn;
		listItem[i].onmouseout = hoverOut;
	}
	
	
	var subButt = document.getElementById('sub');
	subButt.onclick = formSuccess;

}
